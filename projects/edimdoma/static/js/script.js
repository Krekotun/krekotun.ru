   $.fn.alignCenter = function() {
	  //get margin top
	  var marginTop =  - $(this).height()/2 + 'px';
	  //return updated element
	  return $(this).css({ 'margin-top':marginTop});
   };

$(document).ready(function(){
	$('#promo-block .places ul li').click(
		function(){
			qIndex = $('#promo-block .places ul>li').index(this);
			$('#promo-block .places').parent().find('li').removeClass('active')
			$(this).addClass('active');
			$('#promo-block .images>img').hide();
			$('#promo-block .images img:eq('+qIndex+')').show();
			return false;
		}
	);
	$('#order .block .items .item .ingredient .open-list').click(
		function(){
			$(this).toggleClass('open-list-active');
			$(this).parent().find('.ingredients').toggle();
			return false;
		}
	);
	$('#order .block .items .item .delete a').click(
		function(){
			$(this).parent().parent().remove();
			return false;
		}
	);
	$('.pop-up-login').height($(document).height());
	$('#header .log-in').click(
		function(){
			$('.pop-up-login').show();
			$('.pop-up-login form').alignCenter();
		}
	);
	$('.pop-up-login form .close a').click(
		function(){
			$('.pop-up-login').hide();
		});
});
