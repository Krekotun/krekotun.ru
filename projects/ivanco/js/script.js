(function($) {
	$.fn.inputDefualts = function(options) {
		var defaults = {
			cl: 'inactive',
			text: this.val()  
		}, 	opts = $.extend(defaults, options);	
		this.addClass(opts['cl']);
		this.val(opts['text']);
		this.focus(function() {
			if($(this).val() == opts['text']) $(this).val('');
			$(this).removeClass(opts['cl']);
		});
		this.blur(function() {
			if($(this).val() == '') {
				$(this).val(opts['text']);
				$(this).addClass(opts['cl']);
			}
		});
	};
})(jQuery);

$(document).ready(function() {
	/* Tooltip */
	$('.new-projects li').hover(
	function(){
			$(this).append('<span class="cloud"><span>'+$(this).find('a').find('img').attr('alt')+'</span><i></i></span>');
			$(this).css({'position' : 'relative'});
		},
	function(){
			$(this).find('.cloud').remove();
			$(this).css({'position' : 'static'});
		}
	);
	$('#name').inputDefualts({text: 'Меня зовут...'});
	$('#email').inputDefualts({text: 'Мой e-mail...'});
	$('#phone').inputDefualts({text: 'Звонить мне можно по телефону...'});
	$('#text').inputDefualts({text: 'Меня интересует такой вопрос...'});
});