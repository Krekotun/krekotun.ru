$(document).ready(function(){

	$('.step .content').css({'display':'none'});
	$('.f-step .content ul').css({'display':'none'});
	$('.step p a.open').attr({'href':'javascript:;'});
	$('.step p a.open').click(function(){
		$('.content').css({'display':'none'});
		$(this).parent().parent().find('.content').css({'display':'block'});
	});

	$('.f-step a.from').click(function(){
		$('.f-step .content ul').css({'display':'none'});
		$('.f-step .content ul.from').css({'display':'block'});
	});

	$('.f-step a.to').click(function(){
		$('.f-step .content ul').css({'display':'none'});
		$('.f-step .content ul.to').css({'display':'block'});
	});

	$('.f-step a.region').click(function(){
		$('.f-step .content ul').css({'display':'none'});
		$('.f-step .content ul.region').css({'display':'block'});
	});

	$('.f-step .content .from li a').click(function(){
		$('input[name="from"]').attr({'value':$(this).attr('rel')});
		$('a.from').html($(this).attr('rel'));
	});

	$('.f-step .content .to li a').click(function(){
		$('input[name="to"]').attr({'value':$(this).attr('rel')});
		$('a.to').html($(this).attr('rel'));
	});

	$('.f-step .content .region li a').click(function(){
		$('input[name="region"]').attr({'value':$(this).attr('rel')});
		$('a.region').html($(this).attr('rel'));
	});

	$('.th-step p a.open').click(function(){

		$(".persons_slider").slider({
			range: "min",
			value: 2,
			min: 1,
			max: 5,
			slide: function(event, ui) {
				if($(".children_slider").slider("value")>0){
					$("a.persons").html('взрослых ' + ui.value + '<span>,&nbsp;</span>детей ' + $(".children_slider").slider("value"));
				}else{
					$("a.persons").html('взрослых ' + ui.value);
				}
			}
		});

		$(".children_slider").slider({
			range: "min",
			value: 0,
			min: 0,
			max: 4,
			slide: function(event, ui) {
				if(ui.value>0){
					$("a.persons").html('взрослых ' + $(".persons_slider").slider("value") + '<span>,&nbsp;</span>детей ' + ui.value);
				}else{
					$("a.persons").html('взрослых ' + $(".persons_slider").slider("value"));
				}
			}
		});

	});
	
	$('.fo-step p a.open').click(function(){

		$(".money_slider").slider({
			range: true,
			min: 0,
			max: 5000,
			step: 100,
			values: [1000, 4000],
			slide: function(event, ui) {
				$('.fo-step p a.open').html($(".money_slider").slider("values", 0) + '$<span>&nbsp;-&nbsp;</span>' + $(".money_slider").slider("values", 1)+'$');
				$('input[name="money_from"]').attr({'value':$(".money_slider").slider("values", 0)});
				$('input[name="money_to"]').attr({'value':$(".money_slider").slider("values", 1)});
			}
		});
		$('.fo-step p a.open').html($(".money_slider").slider("values", 0) + '$ <span>&nbsp;-&nbsp;</span>' + $(".money_slider").slider("values", 1)+'$');

	});
	$('.menu li').hover(
		function(){
			$(this).addClass('active');
		},
		function(){
			$(this).removeClass('active');
		}
	);

	$('.s-step a.calendar').click(function(){
		$('.s-step .content div.days').css({'display':'none'});
		$('.s-step .content div.calendar').css({'display':'block'});
	});

	$('.s-step a.days').click(function(){
		$('.s-step .content div.calendar').css({'display':'none'});
		$('.s-step .content div.days').css({'display':'block'});

		$(".days_slider").slider({
			range: true,
			min: 1,
			max: 21,
			step: 1,
			values: [7, 10],
			slide: function(event, ui) {
				$('a.days_from').html(ui.values[0]);
				$('a.days_to').html(ui.values[1]);
			}
		});


	});
	
	function set_date(){

		var datestr = $('#date').DatePickerGetDate(true);
		var from = datestr[0].split(' ');
		if(from[1]=='01'){ var month = 'Января'; }
		if(from[1]=='02'){ var month = 'Февраля'; }
		if(from[1]=='03'){ var month = 'Марта'; }
		if(from[1]=='04'){ var month = 'Апреля'; }
		if(from[1]=='05'){ var month = 'Мая'; }
		if(from[1]=='06'){ var month = 'Июня'; }
		if(from[1]=='07'){ var month = 'Июля'; }
		if(from[1]=='08'){ var month = 'Августа'; }
		if(from[1]=='09'){ var month = 'Сентября'; }
		if(from[1]=='10'){ var month = 'Октября'; }
		if(from[1]=='11'){ var month = 'Ноября'; }
		if(from[1]=='12'){ var month = 'Декабря'; }
		if(from[2]=='01'){ from[2] = '1'; }
		if(from[2]=='02'){ from[2] = '2'; }
		if(from[2]=='03'){ from[2] = '3'; }
		if(from[2]=='04'){ from[2] = '4'; }
		if(from[2]=='05'){ from[2] = '5'; }
		if(from[2]=='06'){ from[2] = '6'; }
		if(from[2]=='07'){ from[2] = '7'; }
		if(from[2]=='08'){ from[2] = '8'; }
		if(from[2]=='09'){ from[2] = '9'; }
		$('.date_from').html(from[2] + ' ' +month);

		var to = datestr[1].split(' ');
		if(to[1]=='01'){ var month = 'Января'; }
		if(to[1]=='02'){ var month = 'Февраля'; }
		if(to[1]=='03'){ var month = 'Марта'; }
		if(to[1]=='04'){ var month = 'Апреля'; }
		if(to[1]=='05'){ var month = 'Мая'; }
		if(to[1]=='06'){ var month = 'Июня'; }
		if(to[1]=='07'){ var month = 'Июля'; }
		if(to[1]=='08'){ var month = 'Августа'; }
		if(to[1]=='09'){ var month = 'Сентября'; }
		if(to[1]=='10'){ var month = 'Октября'; }
		if(to[1]=='11'){ var month = 'Ноября'; }
		if(to[1]=='12'){ var month = 'Декабря'; }
		if(to[2]=='01'){ to[2] = '1'; }
		if(to[2]=='02'){ to[2] = '2'; }
		if(to[2]=='03'){ to[2] = '3'; }
		if(to[2]=='04'){ to[2] = '4'; }
		if(to[2]=='05'){ to[2] = '5'; }
		if(to[2]=='06'){ to[2] = '6'; }
		if(to[2]=='07'){ to[2] = '7'; }
		if(to[2]=='08'){ to[2] = '8'; }
		if(to[2]=='09'){ to[2] = '9'; }
		$('.date_to').html(to[2] + ' ' +month);

	}
	
	$('#date').DatePicker({
		flat: true,
		date: ['2008 07 28','2008 07 31'],
		current: '2008 07 31',
		calendars: 2,
		format: 'y m d',
		mode: 'range',
		starts: 1,
		onChange: function(){ set_date(); }
	});

});