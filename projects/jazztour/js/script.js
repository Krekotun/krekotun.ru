$(document).ready(function(){
	$('.log-in a').click(
		function(){
			$('.login-popup').css({'display' : 'block'});
			return false;
		}
	);
	$('.login-popup .close').click(
		function(){
			$('.login-popup').css({'display' : 'none'});
			return false;
		}
	);
	$('div.office .city .selected a').click(
		function(){
			$('div.office .city').toggleClass('city-active');
			return false;
		}
	);
	$('div.office .city .list a').click(
		function(){
			if ( $('div.office').hasClass('petersburg') ) {
				$('div.office').removeClass('petersburg').addClass('moscow')
			} else {
				$('div.office').removeClass('moscow').addClass('petersburg')
			}
			$('div.office .city').removeClass('city-active');
		}
	);
	$('.sidebar .online-booking').hover(
		function(){
			$(this).addClass('online-booking-hover');
		},
		function(){
			$(this).removeClass('online-booking-hover');
		}
	);
	$('.special-offers-list>li').hover(
		function(){
			$(this).addClass('hover');
			$(this).parent().find('li').removeClass('online-booking-hover');
		},
		function(){
			$(this).removeClass('hover');
		}
	);
	$('.special-offers .sort > li > a').click(
		function(){
			$('.special-offers .sort > li').removeClass('active');
			$(this).parent().addClass('active');
			return false;
		}
	);
	$('.online-booking-link').click(
		function(){
			$('.country-online-booking').show();
		}
	);
	/* ONLINE-BOOKING */
	$('.sidebar .online-booking .items>li').click(
		function(){
			var opening_class = $(this).find('a').attr('href');
			if($('.blocks>form.' + opening_class).css('display')=='none'){
				$('.online-booking').addClass('online-booking-active');
				$('.online-booking .items').find('li').removeClass('active')
				$(this).addClass('active');
				$('.blocks>form').css({'display':'none'});
				//$('.blocks>form.' + opening_class).css({'display':'block'});
				$('.online-booking > .block > .bottom').css({'display':'none'});
				$('.blocks>form.' + opening_class).animate({'height':'show'},200,function(){
					$('.online-booking > .block > .bottom').css({'display':'block'});
				});
			}else{
				$('.online-booking').removeClass('online-booking-active');
				$('.online-booking .items').find('li').removeClass('active')
				$(this).removeClass('active');
				//$('.blocks>form').css({'display':'none'});
				$('.online-booking > .block > .bottom').css({'display':'none'});
				$('.blocks>form.' + opening_class).animate({'height':'hide'},200,function(){
					$('.online-booking > .block > .bottom').css({'display':'block'});
				});
			}
			return false;
		}
	);
	$('.country-online-booking .items>li').click(
		function(){
			var opening_class = $(this).find('a').attr('href');
			if($('.blocks>form.' + opening_class).css('display')=='none'){
				$('.online-booking').addClass('online-booking-active');
				$('.online-booking .items').find('li').removeClass('active')
				$(this).addClass('active');
				$('.blocks>form').css({'display':'none'});
				//$('.blocks>form.' + opening_class).css({'display':'block'});
				$('.online-booking > .block > .bottom').css({'display':'none'});
				$('.blocks>form.' + opening_class).animate({'height':'show'},200,function(){
					$('.online-booking > .block > .bottom').css({'display':'block'});
				});
			}else{
				$('.online-booking .items').find('li').removeClass('active')
				$(this).removeClass('active');
				//$('.blocks>form').css({'display':'none'});
				$('.online-booking > .block > .bottom').css({'display':'none'});
				$('.blocks>form.' + opening_class).animate({'height':'hide'},200,function(){
					$('.online-booking > .block > .bottom').css({'display':'block'});
				});
			}
			return false;
		}
	);
	$('.sidebar .online-booking .close').click(
		function(){
			$('.online-booking .blocks .item').css({'display' : 'none'});
			$('.online-booking .items>li').removeClass('active');
			$('.online-booking').removeClass('online-booking-active');
			return false;
		}
	);
	$('.country-online-booking .close').click(
		function(){
			$('.country-online-booking').hide();
			$('.online-booking .items>li').removeClass('active');
			return false;
		}
	);
	if($('.tour .hidden').length){
		$('.tour .input.country a.label').click(function(){
			if($('.tour .hidden.country').css('display')=='none'){
				$('.online-booking > .block > .bottom').css({'display':'none'});
				$('.tour .hidden.country').animate({'height':'show'},300,function(){
					$('.online-booking > .block > .bottom').css({'display':'block'});
				});
			}else{
				$('.online-booking > .block > .bottom').css({'display':'none'});
				$('.tour .hidden.country').animate({'height':'hide'},300,function(){
					$('.online-booking > .block > .bottom').css({'display':'block'});
				});
			}
			return false;
		});
		$('.tour .input.date a.label').click(function(){
			if($('.tour .hidden.date').css('display')=='none'){
				//$('.tour .hidden.date').css({'display':'block'});
				$('.online-booking > .block > .bottom').css({'display':'none'});
				/* default values */
				$('.tour .hidden.date').animate({'height':'show'},300,function(){
					$('.online-booking > .block > .bottom').css({'display':'block'});
				});
			}else{
				$('.online-booking > .block > .bottom').css({'display':'none'});
				$('.tour .hidden.date').animate({'height':'hide'},300,function(){
					$('.online-booking > .block > .bottom').css({'display':'block'});
				});
			}
			return false;
		});
		$('.ticket .input.flight a.label').click(function(){
			if($('.ticket .hidden.flight').css('display')=='none'){
				$('.online-booking > .block > .bottom').css({'display':'none'});
				$('.ticket .hidden.flight').animate({'height':'show'},300,function(){
					$('.online-booking > .block > .bottom').css({'display':'block'});
				});
			}else{
				$('.online-booking > .block > .bottom').css({'display':'none'});
				$('.ticket .hidden.flight').animate({'height':'hide'},300,function(){
					$('.online-booking > .block > .bottom').css({'display':'block'});
				});
			}
			return false;
		});
		$('.ticket .input.direction a.label').click(function(){
			if($('.ticket .hidden.direction').css('display')=='none'){
				$('.online-booking > .block > .bottom').css({'display':'none'});
				$('.ticket .hidden.direction').animate({'height':'show'},300,function(){
					$('.online-booking > .block > .bottom').css({'display':'block'});
				});
			}else{
				$('.online-booking > .block > .bottom').css({'display':'none'});
				$('.ticket .hidden.direction').animate({'height':'hide'},300,function(){
					$('.online-booking > .block > .bottom').css({'display':'block'});
				});
			}
			return false;
		});
		$('.ticket .input.date a.label').click(function(){
			if($('.ticket .hidden.date').css('display')=='none'){
				$('.online-booking > .block > .bottom').css({'display':'none'});
				$('.ticket .hidden.date').animate({'height':'show'},300,function(){
					$('.online-booking > .block > .bottom').css({'display':'block'});
				});
			}else{
				$('.online-booking > .block > .bottom').css({'display':'none'});
				$('.ticket .hidden.date').animate({'height':'hide'},300,function(){
					$('.online-booking > .block > .bottom').css({'display':'block'});
				});
			}
			return false;
		});
		$('.hotel .input.type a.label').click(function(){
			if($('.hotel .hidden.type').css('display')=='none'){
				$('.online-booking > .block > .bottom').css({'display':'none'});
				$('.hotel .hidden.type').animate({'height':'show'},300,function(){
					$('.online-booking > .block > .bottom').css({'display':'block'});
				});
			}else{
				$('.online-booking > .block > .bottom').css({'display':'none'});
				$('.hotel .hidden.type').animate({'height':'hide'},300,function(){
					$('.online-booking > .block > .bottom').css({'display':'block'});
				});
			}
			return false;
		});
		$('.hotel .input.date a.label').click(function(){
			if($('.hotel .hidden.date').css('display')=='none'){
				$('.online-booking > .block > .bottom').css({'display':'none'});
				$('.hotel .hidden.date').animate({'height':'show'},300,function(){
					$('.online-booking > .block > .bottom').css({'display':'block'});
				});
			}else{
				$('.online-booking > .block > .bottom').css({'display':'none'});
				$('.hotel .hidden.date').animate({'height':'hide'},300,function(){
					$('.online-booking > .block > .bottom').css({'display':'block'});
				});
			}
			return false;
		});
		$('.tour .hidden.date .save.ok a').click(function(){
			$('.online-booking > .block > .bottom').css({'display':'none'});
			$('.tour .hidden.date').animate({'height':'hide'},300,function(){
				$('.online-booking > .block > .bottom').css({'display':'block'});
			});
			$('.tour .input.date .value .default').css({'display':'none'});
			if($('.tour .hidden.date #flight').val() != ""){
				$('.tour .input.date .value .c-value .flight-value').html('Вылет: ' + $('.tour .hidden.date #flight').val() + ', ');
			}
			$('.tour .input.date .value .c-value .from-days-value').html('от: ' + $('select[name="from-days"] option:selected').val());
			$('.tour .input.date .value .c-value .to-days-value').html(', до: ' + $('select[name="to-days"] option:selected').val());
			$('.tour .input.date .value .c-value .grafik-value').html(', график: ' + $('.tour .hidden.date .line-input .line-value').html());
			return false;
		});
		$('.ticket .hidden.flight .save.ok a').click(function(){
			$('.online-booking > .block > .bottom').css({'display':'none'});
			$('.ticket .hidden.flight').animate({'height':'hide'},300,function(){
				$('.online-booking > .block > .bottom').css({'display':'block'});
			});
			$('.ticket .input.flight .value .default').css({'display':'none'});
			$('.ticket .input.flight .value .direction-value').html('Направление: ' + $('.ticket .hidden.flight input:radio[name="direction"]:checked').val());
			$('.ticket .input.flight .value .class-value').html(', класс: ' + $('select[name="class"] option:selected').val());
			if($('#transplantation:checked').length){
				$('.ticket .input.flight .value .transplantation-value').html(', ' + $('#transplantation').val());
			}else{
				$('.ticket .input.flight .value .transplantation-value').html('');
			}
//			$('.tour .input.date .value .c-value .to-days-value').html(', до: ' + $('select[name="to-days"] option:selected').val());
//			$('.tour .input.date .value .c-value .grafik-value').html(', график: ' + $('.tour .hidden.date .line-input .line-value').html());
			return false;
		});
		$('.ticket .hidden.direction .save.ok a').click(function(){
			$('.online-booking > .block > .bottom').css({'display':'none'});
			$('.ticket .hidden.direction').animate({'height':'hide'},300,function(){
				$('.online-booking > .block > .bottom').css({'display':'block'});
			});
			$('.ticket .input.direction .value .default').css({'display':'none'});
			$('.ticket .input.direction .value .direction-from-value').html('Откуда: ' + $('select[name="direction-from"] option:selected').val() +  ', ');
			$('.ticket .input.direction .value .direction-to-value').html('куда: ' + $('select[name="direction-to"] option:selected').val());
			return false;
		});
		$('.ticket .hidden.date .save.ok a').click(function(){
			$('.online-booking > .block > .bottom').css({'display':'none'});
			$('.ticket .hidden.date').animate({'height':'hide'},300,function(){
				$('.online-booking > .block > .bottom').css({'display':'block'});
			});
			$('.ticket .input.date .value .default').css({'display':'none'});
			if($('.ticket .hidden.date #ticket-there').val() != ""){
				$('.ticket .input.date .value .ticket-there-value').html('Туда: ' + $('.ticket .hidden.date #ticket-there').val() + ', ');
			}
			if($('.ticket .hidden.date #ticket-back').val() != ""){
				$('.ticket .input.date .value .ticket-back-value').html('Обратно: ' + $('.ticket .hidden.date #ticket-back').val() + ', ');
			}
			$('.ticket .input.date .value .grafik-value').html('график: ' + $('.ticket .hidden.date .line-input .line-value').html());
			return false;
		});
		$('.hidden.date .line .line-input .more').click(function(){
			var line_value = parseInt($(this).parent().find('.line-value').html());
			if(line_value != 5){
				$(this).parent().find('.line-value').html(line_value + 1);
			}
			return false;
		});
		$('.hidden.date .line .line-input .less').click(function(){
			var line_value = parseInt($(this).parent().find('.line-value').html());
			if(line_value != 1){
				$(this).parent().find('.line-value').html(line_value - 1);
			}
			return false;
		});
		$('.hotel .hidden.type .save.ok a').click(function(){
			$('.online-booking > .block > .bottom').css({'display':'none'});
			$('.hotel .hidden.type').animate({'height':'hide'},300,function(){
				$('.online-booking > .block > .bottom').css({'display':'block'});
			});
			return false;
		});
		$('.hotel .hidden.date .save.ok a').click(function(){
			$('.online-booking > .block > .bottom').css({'display':'none'});
			$('.hotel .hidden.date').animate({'height':'hide'},300,function(){
				$('.online-booking > .block > .bottom').css({'display':'block'});
			});
			$('.hotel .input.date .value .default').css({'display':'none'});
			$('.hotel .input.date .value .stopover-value').html('Заезд: ' + $('.hotel .hidden.date #stopover').val());
			if($('.hotel .hidden.date #departure').val() != ""){
				$('.hotel .input.date .value .departure-value').html(', выезд: ' + $('.hotel .hidden.date #departure').val());
			}
			return false;
		});
		$('.tour .hidden.country .save.ok a').click(function(){
			$('.online-booking > .block > .bottom').css({'display':'none'});
			$('.tour .hidden.country').animate({'height':'hide'},300,function(){
				$('.online-booking > .block > .bottom').css({'display':'block'});
			});
			return false;
		});
		$('.hotel .hidden.type .save.cancel a').click(function(){
			$('.hotel .input.type .value .selected-item').css({'display':'none'});
			$('.hotel .input.type .value .default').css({'display':'inline'});
			$('.hotel .hidden.type ul li').removeClass('selected');
			$('.online-booking > .block > .bottom').css({'display':'none'});
			$('.hotel .hidden.type ul li').animate({'height':'show'},300,function(){
				$('.online-booking > .block > .bottom').css({'display':'block'});
			});
			return false;
		});
		$('.tour .hidden.country .save.cancel a').click(function(){
			$('.tour .input.country .value .selected-item').css({'display':'none'});
			$('.tour .input.country .value .default').css({'display':'inline'});
			$('.tour .hidden.country ul li').removeClass('selected');
			$('.online-booking > .block > .bottom').css({'display':'none'});
			$('.online-booking > .block > .bottom').animate({'height':'show'},300,function(){
				$('.online-booking > .block > .bottom').css({'display':'block'});
			});
			return false;
		});
		$('.hotel .hidden.type ul li a').each(function(){
			var li_index = $('.hotel .hidden.type ul li').index($(this).parent());
			$(this).parent().attr({'rel':li_index});
			$(this).html('<span class="link">' + $(this).html() + '</span><span class="left-top"></span><span class="right-top"></span><span class="left-bottom"></span><span class="right-bottom"></span>');
			$('.hotel .input.type .value').html($('.hotel .input.type .value').html() + '<span class="selected-item" rel="' + $('.hotel .hidden.type ul li').index($(this).parent()) + '">' + $(this).find('.link').html().replace(' ','&nbsp;').replace(' ','&nbsp;') + '<a href="#" class="close"><img src="./css/i/delete-selected-item.png" alt="" /></a></span>');
		});
		$('.tour .hidden.country ul li a').each(function(){
			var li_index = $('.tour .hidden.country ul li').index($(this).parent());
			$(this).parent().attr({'rel':li_index});
			$(this).html('<span class="link">' + $(this).html() + '</span><span class="left-top"></span><span class="right-top"></span><span class="left-bottom"></span><span class="right-bottom"></span>');
			$('.tour .input.country .value').html($('.tour .input.country .value').html() + '<span class="selected-item" rel="' + $('.tour .hidden.country ul li').index($(this).parent()) + '">' + $(this).find('.link').html().replace(' ','&nbsp;').replace(' ','&nbsp;') + '<a href="#" class="close"><img src="./css/i/delete-selected-item.png" alt="" /></a></span>');
		});
		$('.hotel .hidden.type ul li a').click(function(){
			var li_index = $('.hotel .hidden.type ul li').index($(this).parent());
			if($(this).parent().hasClass('selected')){
				// removing
				$('.hotel .input.type .value .selected-item[rel="' + li_index + '"]').css({'display':'none'});
				$(this).parent().removeClass('selected');
				var show_default = 1;
				$('.hotel .input.type .value .selected-item').each(function(){
					if($(this).css('display')=='inline'){
						show_default = 0;
					}
				});
				if(show_default == 1){
					$('.hotel .input.type .value .default').css({'display':'inline'});
				}
				$('.online-booking > .block > .bottom').css({'display':'none'});
				$('.hotel .input.type .value .selected-item[rel="' + li_index + '"]').animate({'height':'hide'},300,function(){
					$('.online-booking > .block > .bottom').css({'display':'block'});
				});
			}else{
				// adding
				$('.hotel .input.type .value .selected-item[rel="' + li_index + '"]').css({'display':'inline'});
				$('.hotel .input.type .value .default').css({'display':'none'});
				$(this).parent().addClass('selected')
				$('.online-booking > .block > .bottom').css({'display':'none'});
				$('.hotel .input.type .value .selected-item[rel="' + li_index + '"]').animate({'height':'show'},300,function(){
					$('.online-booking > .block > .bottom').css({'display':'block'});
				});
			}
			return false;
		});
		$('.tour .hidden.country ul li a').click(function(){
			var li_index = $('.tour .hidden.country ul li').index($(this).parent());
			if($(this).parent().hasClass('selected')){
				// removing
				$('.tour .input.country .value .selected-item[rel="' + li_index + '"]').css({'display':'none'});
				$(this).parent().removeClass('selected');
				var show_default = 1;
				$('.tour .input.country .value .selected-item').each(function(){
					if($(this).css('display')=='inline'){
						show_default = 0;
					}
				});
				if(show_default == 1){
					$('.tour .input.country .value .default').css({'display':'inline'});
				}
				$('.online-booking > .block > .bottom').css({'display':'none'});
				$('.online-booking > .block > .bottom').animate({'height':'hide'},300,function(){
					$('.online-booking > .block > .bottom').css({'display':'block'});
				});
			}else{
				// adding
				$('.tour .input.country .value .selected-item[rel="' + li_index + '"]').css({'display':'inline'});
				$('.tour .input.country .value .default').css({'display':'none'});
				$(this).parent().addClass('selected');
				$('.online-booking > .block > .bottom').css({'display':'none'});
				$('.online-booking > .block > .bottom').animate({'height':'show'},300,function(){
					$('.online-booking > .block > .bottom').css({'display':'block'});
				});
			}
			return false;
		});
		$('.tour .input.country .value .close').click(function(){
			$(this).parent().css({'display':'none'});
			$('.tour .hidden.country ul li[rel="' + $(this).parent().attr('rel') + '"]').removeClass('selected');
			var show_default = 1;
			$('.tour .input.country .value .selected-item').each(function(){
				if($(this).css('display')=='inline'){
					show_default = 0;
				}
			});
			if(show_default == 1){
				$('.tour .input.country .value .default').css({'display':'inline'});
			}
			$('.online-booking > .block > .bottom').css({'display':'none'});
			$('.online-booking > .block > .bottom').animate({'height':'hide'},300,function(){
				$('.online-booking > .block > .bottom').css({'display':'block'});
			});
			return false;
		});
		$('.hotel .input.type .value .close').click(function(){
			$(this).parent().css({'display':'none'});
			$('.hotel .hidden.type ul li[rel="' + $(this).parent().attr('rel') + '"]').removeClass('selected');
			var show_default = 1;
			$('.hotel .input.type .value .selected-item').each(function(){
				if($(this).css('display')=='inline'){
					show_default = 0;
				}
			});
			if(show_default == 1){
				$('.hotel .input.type .value .default').css({'display':'inline'});
			}
			$('.online-booking > .block > .bottom').css({'display':'none'});
			$('.online-booking > .block > .bottom').animate({'height':'hide'},300,function(){
				$('.online-booking > .block > .bottom').css({'display':'block'});
			});
			return false;
		});
	}
	var $tourArr = $('.promo-tours>.promo-tour'),
		tourArrLength = $tourArr.length,
		randNum = Math.floor ( Math.random ( ) * tourArrLength + 1 ),
		list_block = 0;
	$tourArr.eq(randNum-1).addClass('promo-tour-selected');
	$('.tour-list li:eq('+(randNum-1)+')').addClass('active');
	iHeight = function(){
		var top = $('.promo-tours .promo-tour-selected .tour-description').innerHeight();
		$('.tour-list').css({'top': (625-(top-126))+'px'});
	}
	iHeight();
	$('.tour-list li a').click(
		function(){
			if(list_block==0){
				var altValue = $(this).attr('href');
				$tourArr.removeClass('promo-tour-selected');
				$('.promo-tours #'+altValue).addClass('promo-tour-selected');
				$('.tour-list li').removeClass('active')
				$(this).parent().addClass('active');
				iHeight();
			}
			return false;
		}
	);
	/* SLIDESHOW */
	show_block = function(){
		$('.promo-tours-animation').animate({'width':'100px'},10000,function(){
			list_block = 1;
			var selected_index = $('.promo-tour').index($('.promo-tour-selected'));
			if(selected_index+1 > $('.promo-tour').size()-1){
				var next_index = 0;
			}else{
				var next_index = selected_index+1;
			}
			var next_node = $('.promo-tour:eq(' + next_index + ')');
			var previous_node = $('.promo-tour:eq(' + selected_index + ')');
			next_node.addClass('promo-tour-moreselected');
			next_node.css({'opacity':'0'});
			$('.tour-list li').removeClass('active')
			$('.tour-list li a[href="' + next_node.attr('id') + '"]').parent().addClass('active');
			next_node.animate({'opacity':'1'},1300,function(){
				previous_node.removeClass('promo-tour-selected');
				next_node.removeClass('promo-tour-moreselected');
				next_node.addClass('promo-tour-selected');
				show_block();
				iHeight();
				list_block = 0;
			});
		});
	}
	show_block();

	/* MAP */
	if($('.map-countries').length){
		$('.map-countries area').click(function(){
			if($(this).hasClass('selected')){
				$('.map-countries .countries-list li, .map-countries area').removeClass('selected');
				$('.map-countries .map').attr({'class':'map'});
			}else{
				$('.map-countries .countries-list li, .map-countries area').removeClass('selected');
				var selected_class = $(this).attr('class');
				$('.map-countries .countries-list li.' + selected_class + ', .map-countries area.' + selected_class).addClass('selected');
				$('.map-countries .map').attr({'class':'map ' + selected_class});
			}
			return false;
		});
		$('.map .close a').click(function(){
			$('.map-countries .countries-list li').removeClass('selected');
			$('.map-countries .map').attr({'class':'map'});
			return false;
		});
		/*$('.map-countries .countries-list li a').click(function(){
			if($(this).parent().hasClass('selected')){
				$('.map-countries .countries-list li, .map-countries area').removeClass('selected');
				$('.map-countries .map').attr({'class':'map'});
			}else{
				$('.map-countries .countries-list li, .map-countries area').removeClass('selected');
				var selected_class = $(this).parent().attr('class');
				$('.map-countries .countries-list li.' + selected_class + ', .map-countries area.' + selected_class).addClass('selected');
				$('.map-countries .map').attr({'class':'map ' + selected_class});
			}
			return false;
		});*/
	}
	/* date picker */
	var currentTime = new Date();
	var month = currentTime.getMonth() + 1;
	var day = currentTime.getDate();
	var year = currentTime.getFullYear();

	$('#flight').DatePicker({
		format:'d.m.Y',
		date: $('#flight').val(),
		current: $('#flight').val(),
		starts: 1,
		position: 'r',
		calendars: 2,
//		mode: 'range',
		onBeforeShow: function(){
			if($('#flight').val()==""){
				$('#flight').attr({'value':day + '.' + month + '.' + year});
			}
			$('#flight').DatePickerSetDate($('#flight').val(), true);
		},
		onChange: function(formated, dates){
			$('#flight').val(formated);
			$('#flight').DatePickerHide();
		}
	});
	$('#ticket-there').DatePicker({
		format:'d.m.Y',
		date: $('#ticket-there').val(),
		current: $('#ticket-there').val(),
		starts: 1,
		position: 'r',
		calendars: 2,
//		mode: 'range',
		onBeforeShow: function(){
			if($('#ticket-there').val()==""){
				$('#ticket-there').attr({'value':day + '.' + month + '.' + year});
			}
			$('#ticket-there').DatePickerSetDate($('#ticket-there').val(), true);
		},
		onChange: function(formated, dates){
			$('#ticket-there').val(formated);
			$('#ticket-there').DatePickerHide();
		}
	});
	$('#ticket-back').DatePicker({
		format:'d.m.Y',
		date: $('#ticket-back').val(),
		current: $('#ticket-back').val(),
		starts: 1,
		position: 'r',
		calendars: 2,
//		mode: 'range',
		onBeforeShow: function(){
			if($('#ticket-back').val()==""){
				$('#ticket-back').attr({'value':day + '.' + month + '.' + year});
			}
			$('#ticket-back').DatePickerSetDate($('#ticket-back').val(), true);
		},
		onChange: function(formated, dates){
			$('#ticket-back').val(formated);
			$('#ticket-back').DatePickerHide();
		}
	});
	$('#stopover').DatePicker({
		format:'d.m.Y',
		date: $('#stopover').val(),
		current: $('#stopover').val(),
		starts: 1,
		position: 'r',
		calendars: 2,
//		mode: 'range',
		onBeforeShow: function(){
			if($('#stopover').val()==""){
				$('#stopover').attr({'value':day + '.' + month + '.' + year});
			}
			$('#stopover').DatePickerSetDate($('#stopover').val(), true);
		},
		onChange: function(formated, dates){
			$('#stopover').val(formated);
			$('#stopover').DatePickerHide();
		}
	});
	$('#departure').DatePicker({
		format:'d.m.Y',
		date: $('#departure').val(),
		current: $('#departure').val(),
		starts: 1,
		position: 'r',
		calendars: 2,
//		mode: 'range',
		onBeforeShow: function(){
			if($('#departure').val()==""){
				$('#departure').attr({'value':day + '.' + month + '.' + year});
			}
			$('#departure').DatePickerSetDate($('#departure').val(), true);
		},
		onChange: function(formated, dates){
			$('#departure').val(formated);
			$('#departure').DatePickerHide();
		}
	});
});
